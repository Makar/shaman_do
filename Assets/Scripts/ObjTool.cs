using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class ObjTool : MonoBehaviour
{
    public Transform[] ojects;
    
    // Start is called before the first frame update

    public bool makenames = false;
    void Start() {

    }

    // Update is called once per frame
    void OnDrawGizmosSelected() {
        if (makenames == true) {
            makenames = false;
            ojects =
                this.gameObject.GetComponentsInChildren<Transform>();
            int c = 0;
            foreach(Transform goTo in ojects) {
               #if UNITY_EDITOR
                if (Selection.Contains(goTo.gameObject)) {
                    goTo.gameObject.name = goTo.gameObject.name + "_" + c;
                    c++;
                }
               #endif
            }
        }
    }
}
