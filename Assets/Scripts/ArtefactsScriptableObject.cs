using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ArtefactsScriptableObject")]
public class ArtefactsScriptableObject : ScriptableObject
{
    public List<ArtefactScriptableObject> Artefacts;
}
