using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class TestMover : MonoBehaviour
{
    public Action<List<int>> OnSpellActivated;
    [SerializeField] private PointListController _pointListController;
    [SerializeField] private GameObject _radiusImage;
    private float _spellRadius = 5f;
    private bool _isSpellActive;

    public float speed = 1;
    public Renderer spr;
    public Rigidbody2D rb;
    public Animator anim;

    public int effectNum;
    public List<GameObject> effectList;
    public PixelPerfectCamera cam;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<Renderer>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        _radiusImage.SetActive(false);
    }
    
    // Update is called once per frame
    
    void Update() {
        bool move = false;
        Vector3 moveVector = transform.position;
        

        if (Input.GetKeyDown("1")) {
            effectNum -= (effectNum > 0) ? 1 : 0;
        }
        
        if (Input.GetKeyDown("2")) {
            effectNum += (effectNum < effectList.Count-1) ? 1 : 0;
        }
        
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) // forward
        {
            Debug.Log("1");
            cam.assetsPPU *= (cam.assetsPPU<256)?2:1;
        } else if (Input.GetAxis("Mouse ScrollWheel") < 0f) // backwards
        {
            cam.assetsPPU /= (cam.assetsPPU > 16) ? 2 : 1; 
        }
        
        if (Input.GetKeyDown(KeyCode.Space)) {
            if (_isSpellActive)
            {
                foreach (GameObject ef in effectList)
                {
                    ef.SetActive(false);
                }
                effectList[effectNum].SetActive(true);
                effectList[effectNum].transform.position = this.transform.position;

                CastSpell();
            }

        }

        if (Input.GetKey("a")) {
            moveVector += new Vector3(-speed, 0, 0);
            move = true;
        }
        if (Input.GetKey("d")) {
            moveVector += new Vector3(speed, 0, 0);
            move = true;
        }
        if (Input.GetKey("w")) {
            moveVector += new Vector3(0, speed, 0);
            move = true;
        }
        if (Input.GetKey("s")) {
            moveVector += new Vector3(0, -speed, 0);
            move = true;
        }
        if (move) {
            anim.speed = 1f;
            rb.MovePosition(moveVector);
            spr.sortingOrder = -(int)(transform.position.y * 10);
        } else {
            anim.speed = 0;
        }
    }

    public void PrepareSpell()
    {
        _radiusImage.SetActive(true);
        _isSpellActive = true;
    }

    private void CastSpell()
    {
        var ids = new List<int>();

        for(int i = 0; i < _pointListController.buildPoints.Length; i++)
        {
            if (Vector2.Distance(_pointListController.buildPoints[i].transform.position, transform.position) <= _spellRadius)
            {
                ids.Add(i);
            }
        }

        OnSpellActivated?.Invoke(ids);

        _radiusImage.SetActive(false);

        _isSpellActive = false;
    }
}
