using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class SpellCard
{
    public int Id { get; private set; }
    public string Info { get; set; }
    public float CataclysmTime { get; private set; }
    public int CountMultiplyerBonus { get; private set; }
    public int CountBonus { get; private set; }
    public float DelayBonus { get; private set; }
    public int Weight { get; private set; }
    public ResourceType ResourceType { get; private set; }

    public Dictionary<ResourceType, int> RequiredResources { get; private set; }

    public SpellCard(int id, string info, float cataclysmTime, int countMultiplyerBonus, int countBonus, float delayBonus, int weight, ResourceType resourceType, Dictionary<ResourceType, int> requiredResources)
    {
        Id = id;
        Info = info;
        CataclysmTime = cataclysmTime;
        CountMultiplyerBonus = countMultiplyerBonus;
        CountBonus = countBonus;
        DelayBonus = delayBonus;
        Weight = weight;
        ResourceType = resourceType;

        RequiredResources = requiredResources;
    }
}
