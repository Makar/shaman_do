using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class Spell
{
    public float CataclysmTime { get; private set; }
    public int CountMultiplyerBonus { get; private set; }
    public int CountBonus { get; private set; }
    public float DelayBonus { get; private set; }
    public Resource Resource { get; private set; }

    private ResourceManager _resourceManager;
    public Dictionary<ResourceType, int> RequiredResources { get; private set; }

    public Spell(ResourceManager resourceManager)
    {
        _resourceManager = resourceManager;
        RequiredResources = new Dictionary<ResourceType, int>();
        DelayBonus = 1;
    }

    public void ApplySpellCard(SpellCard spellCard)
    {
        CataclysmTime += spellCard.CataclysmTime;
        CountMultiplyerBonus += spellCard.CountMultiplyerBonus;
        CountBonus += spellCard.CountBonus;
        DelayBonus *= spellCard.DelayBonus;

        if (spellCard.ResourceType != ResourceType.Undefined)
        {
            Resource = _resourceManager.GetResourceByType(spellCard.ResourceType);
        }

        foreach(var requiredResource in spellCard.RequiredResources)
        {
            if (RequiredResources.ContainsKey(requiredResource.Key))
            {
                RequiredResources[requiredResource.Key] += requiredResource.Value;
            }
            else
            {
                RequiredResources.Add(requiredResource.Key, requiredResource.Value);
            }
        }
    }
}
