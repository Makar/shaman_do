using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;
using System.Linq;

public class GameManager
{
    public Action<Building.BuildingType, int, int, ResourceType> OnBuildingCreated;
    public Action<ResourceType, int, int> OnBuildingUpdated;
    public Action<int> OnBuildingRemoved;
    public Action<ResourceType, int> OnResourceUpdated;
    public Action OnHumanCreated;
    public Action OnHumanRemoved;
    public Action<int, string, float, int, int, float, ResourceType, Dictionary<ResourceType, int>> OnCardCreated;
    public Action<float> OnShowResults;
    public Action StartCataclysm;
    public Action<List<int>> OnSpellCast;

    private Map _map;
    private ResourceManager _resourceManager;
    private SpellsManager _spellsManager;
    private ArtefactsManager _artefactsManager;
    private CataclysmManager _cataclysmManager;
    private Spell _currentSpell;

    private Queue<Building.BuildingType> _predefinedBuildingTypes;
    private Queue<ResourceType> _predefinedBuildingResources;

    private float _resultTimer;

    public GameManager(PointListController pointListController, 
                        SpellCardsScriptableObject spellsScriptableObject,
                        ArtefactsScriptableObject artefactsScriptableObject,
                        IngredientsScriptableObject ingredientsScriptableObject)
    {
        _map = new Map(pointListController.buildPoints.Length);
        _resourceManager = new ResourceManager(OnResourceEarned, OnResourceSpent);
        _cataclysmManager = new CataclysmManager(this);
        _spellsManager = new SpellsManager(_resourceManager, _cataclysmManager, spellsScriptableObject);
        _artefactsManager = new ArtefactsManager(artefactsScriptableObject, ingredientsScriptableObject);

        _predefinedBuildingTypes = new Queue<Building.BuildingType>();
        _predefinedBuildingResources = new Queue<ResourceType>();
    }

    public void Init()
    {
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Hut);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Bonfire);
        _predefinedBuildingTypes.Enqueue(Building.BuildingType.Bonfire);

        _predefinedBuildingResources.Enqueue(ResourceType.Meat);
        _predefinedBuildingResources.Enqueue(ResourceType.Mushrooms);
        _predefinedBuildingResources.Enqueue(ResourceType.Stone);
        _predefinedBuildingResources.Enqueue(ResourceType.Wood);
        _predefinedBuildingResources.Enqueue(ResourceType.Building);
        _predefinedBuildingResources.Enqueue(ResourceType.Fish);

        _resourceManager.GetResourceByType(ResourceType.Building).EarnResource(8);
        _resourceManager.GetResourceByType(ResourceType.SpellCard).EarnResource(3);

        _resourceManager.GetResourceByType(ResourceType.Fish).EarnResource(10);
        _resourceManager.GetResourceByType(ResourceType.Meat).EarnResource(10);
        _resourceManager.GetResourceByType(ResourceType.Mushrooms).EarnResource(10);
        _resourceManager.GetResourceByType(ResourceType.Stone).EarnResource(10);
        _resourceManager.GetResourceByType(ResourceType.Wood).EarnResource(10);
    }

    public void Tick()
    {
        foreach(var mapCell in _map.GetCells())
        {
            var building = mapCell.GetBuilding();

            if (building != null)
            {
                building.Update(Time.deltaTime);
            }
        }

        _cataclysmManager.Update(Time.deltaTime);

        _resultTimer += Time.deltaTime;
    }

    public CataclysmManager GetCataclysmManager()
    {
        return _cataclysmManager;
    }

    private void OnResourceEarned(ResourceType resourceType, int count)
    {
        if (resourceType == ResourceType.Building)
        {
            for(int i = 0; i < count; i++)
            {
                var cells = _map.GetCells().Where(x => !x.IsBusy).ToList();


                if (cells.Count == 0)
                {
                    break;
                }

                int index = UnityEngine.Random.Range(0, cells.Count);
                var cell = cells[index];

                Building.BuildingType buildingType = UnityEngine.Random.Range(0, 6) == 0 ? Building.BuildingType.Bonfire : Building.BuildingType.Hut;

                if (_predefinedBuildingTypes.Count > 0)
                {
                    buildingType = _predefinedBuildingTypes.Dequeue();
                }

                if (buildingType == Building.BuildingType.Hut)
                {
                    var building = new Building(buildingType, _resourceManager.GetResourceByType(GetRandomResourceType()));
                    cell.SetBuilding(building); ;
                    cell.GetBuilding().AddHuman(new Human());

                    _resourceManager.EarnResource(ResourceType.Human, 1, false);

                    OnResourceUpdated?.Invoke(ResourceType.Human, _resourceManager.GetResourceByType(ResourceType.Human).GetCount());
                    OnHumanCreated?.Invoke();
                    OnBuildingCreated?.Invoke(buildingType, cell.Index, building.GetHumansCount(), building.GetResourceType());
                }
                else
                {
                    var building = new Building(buildingType, _resourceManager.GetResourceByType(ResourceType.SpellCard));
                    cell.SetBuilding(building);
                    OnBuildingCreated?.Invoke(buildingType, cell.Index, building.GetHumansCount(), building.GetResourceType());
                }

            }
        }

        if (resourceType == ResourceType.Human)
        {
            for (int i = 0; i < count; i++)
            {
                var cells = _map.GetCells().Where(x => x.IsBusy && !x.GetBuilding().IsFull).ToList();

                if (cells.Count == 0)
                {
                    _resourceManager.GetResourceByType(ResourceType.Human).SpendResource(count - i, false);
                    break;
                }

                var cell = cells[UnityEngine.Random.Range(0, cells.Count)];
                var building = cell.GetBuilding();
                cell.GetBuilding().AddHuman(new Human());
                OnHumanCreated?.Invoke();

                OnBuildingUpdated?.Invoke(building.GetResourceType(), cell.Index, building.GetHumansCount());
            }
        }

        if (resourceType == ResourceType.SpellCard)
        {
            for (int i = 0; i < count; i++)
            {
                var card = _spellsManager.AddRandomCard();

                OnCardCreated(card.Id, card.Info, card.CataclysmTime, card.CountBonus, card.CountMultiplyerBonus, card.DelayBonus, card.ResourceType, card.RequiredResources);
            }


        }

        OnResourceUpdated?.Invoke(resourceType, _resourceManager.GetResourceByType(resourceType).GetCount());
    }

    public void OnResourceSpent(ResourceType resourceType, int count)
    {
        if (resourceType == ResourceType.Human)
        {
            for(int i = 0; i < count; i++)
            {
                var cells = _map.GetCells().Where(x => x.IsBusy).ToList();

                if (cells.Count == 0)
                {
                    break;
                }

                var cell = cells[UnityEngine.Random.Range(0, cells.Count)];
                var building = cell.GetBuilding();
                building.RemoveHuman();

                OnHumanRemoved?.Invoke();

                OnBuildingUpdated?.Invoke(building.GetResourceType(), cell.Index, building.GetHumansCount());
            }
        }
        OnResourceUpdated?.Invoke(resourceType, _resourceManager.GetResourceByType(resourceType).GetCount());
    }

    public void DestroyBuildings(int count)
    {
        StartCataclysm?.Invoke();

        for (int i = 0; i < count; i++)
        {
            var cells = _map.GetCells().Where(x => x.IsBusy).ToList();

            if (cells.Count == 0)
            {
                break;
            }

            var cell = cells[UnityEngine.Random.Range(0, cells.Count)];
            var building = cell.GetBuilding();
            cell.RemoveBuilding();
            var humansCount = building.GetHumansCount();
            building.RemoveAll();
            _resourceManager.GetResourceByType(ResourceType.Human).SpendResource(humansCount);

            OnBuildingRemoved?.Invoke(cell.Index);
        }

        if (_map.GetCells().Where(x => x.IsBusy).ToList().Count == 0)
        {
            ShowResults();
        }
    }

    public void SpedResources(Dictionary<ResourceType, int> resources)
    {
        foreach(var resource in resources)
        {
            _resourceManager.GetResourceByType(resource.Key).SpendResource(resource.Value);
        }
    }

    private void ShowResults()
    {
        Time.timeScale = 0f;
        OnShowResults?.Invoke(_resultTimer);
    }

    public Map GetMap()
    {
        return _map;
    }

    public void CreateSpell(List<int> ids)
    {
        _currentSpell = _spellsManager.CreateSpell(ids);
    }

    public void CastSpell(List<int> cells)
    {
        var mapCells = new List<MapCell>();

        foreach (var cellIndex in cells)
        {
            mapCells.Add(_map.GetCell(cellIndex));
        }

        if (_currentSpell != null)
        {
            _spellsManager.CastSpell(mapCells, _currentSpell);

            foreach(var requiredResource in _currentSpell.RequiredResources)
            {
                _resourceManager.SpendResource(requiredResource.Key, requiredResource.Value);
            }
        }

        foreach(var mapCell in mapCells)
        {
            var building = mapCell.GetBuilding();
            if (building != null)
            {
                OnBuildingUpdated?.Invoke(building.GetResourceType(), mapCell.Index, building.GetHumansCount());
            }
        }

        OnSpellCast?.Invoke(cells);

        _currentSpell = null;
    }

    private ResourceType GetRandomResourceType()
    {
        if (_predefinedBuildingResources.Count > 0)
        {
            return _predefinedBuildingResources.Dequeue();
        }

        var count = Enum.GetNames(typeof(ResourceType)).Length;

        return (ResourceType)UnityEngine.Random.Range(2, count);
    }
}
