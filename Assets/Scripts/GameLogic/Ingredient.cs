using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ingredient 
{
    public enum IngredientType
    {
        Stick,
        Skull,
        Bone,
        BearTooth
    }

    public string Name { get; private set; }
    public string Description { get; private set; }
    public Sprite Icon { get; private set; }

    public Ingredient(string name, string description, Sprite icon)
    {
        Name = name;
        description = Description;
        Icon = icon;
    }
}
