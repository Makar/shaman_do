using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class Resource
{
    public ResourceType ResourceType { get; private set; }

    private int _count;
    private int _baseIncome;
    private Action<ResourceType, int> _earnCallBack;
    private Action<ResourceType, int> _spendCallBack;

    public Resource(ResourceType resourceType, Action<ResourceType, int> earnCallBack, Action<ResourceType, int> spendCallBack)
    {
        ResourceType = resourceType;
        _baseIncome = 1;
        _earnCallBack = earnCallBack;
        _spendCallBack = spendCallBack;
    }

    public int GetCount()
    {
        return _count;
    }

    public int GetBaseIncome()
    {
        return _baseIncome;
    }

    public void EarnResource(int value, bool needCallback = true)
    {
        _count += value;
        if (needCallback)
        {
            _earnCallBack?.Invoke(ResourceType, value);
        }
    }

    public void SpendResource(int value, bool needCallback = true)
    {
        _count = Mathf.Max(0, _count - value);
        if (needCallback)
        {
            _spendCallBack?.Invoke(ResourceType, value);
        }
    }
}
