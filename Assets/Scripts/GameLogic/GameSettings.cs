using LD50.Resources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings
{
    private static GameSettings _instance;

    private Dictionary<ResourceType, float> _resourcesBaseDelay;
    private Dictionary<ResourceType, float> _resourcesBaseMultiplyer;

    public static GameSettings GetInstance()
    {
        if (_instance == null)
        {
            _instance = new GameSettings();
        }

        return _instance;
    }

    private GameSettings()
    {
        _resourcesBaseDelay = new Dictionary<ResourceType, float>();
        _resourcesBaseMultiplyer = new Dictionary<ResourceType, float>();

        _resourcesBaseDelay.Add(ResourceType.Building, 60f);
        _resourcesBaseDelay.Add(ResourceType.Human, 30f);
        _resourcesBaseDelay.Add(ResourceType.Meat, 15f);
        _resourcesBaseDelay.Add(ResourceType.Fish, 15f);
        _resourcesBaseDelay.Add(ResourceType.Wood, 20f);
        _resourcesBaseDelay.Add(ResourceType.Mushrooms, 15f);
        _resourcesBaseDelay.Add(ResourceType.Stone, 20f);
        _resourcesBaseDelay.Add(ResourceType.SpellCard, 60f);

        _resourcesBaseMultiplyer.Add(ResourceType.Building, 0.1f);
        _resourcesBaseMultiplyer.Add(ResourceType.Human, 0.1f);
        _resourcesBaseMultiplyer.Add(ResourceType.Meat, 1f);
        _resourcesBaseMultiplyer.Add(ResourceType.Wood, 1f);
        _resourcesBaseMultiplyer.Add(ResourceType.Mushrooms, 1f);
        _resourcesBaseMultiplyer.Add(ResourceType.Stone, 1f);
        _resourcesBaseMultiplyer.Add(ResourceType.SpellCard, 1f);
    }

    public float GetDelayByType(ResourceType resourceType)
    {
        if (_resourcesBaseDelay.TryGetValue(resourceType, out var value))
        {
            return value;
        }

        return 10f;
    }

    public float GetMultiplyerByType(ResourceType resourceType)
    {
        if (_resourcesBaseMultiplyer.TryGetValue(resourceType, out var value))
        {
            return value;
        }

        return 1f;
    }

}
