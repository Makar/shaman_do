using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class ResourceManager
{
    private Dictionary<ResourceType, Resource> _resources;

    public ResourceManager(Action<ResourceType, int> earnCallBack, Action<ResourceType, int> spendCallBack)
    {
        _resources = new Dictionary<ResourceType, Resource>();

        _resources.Add(ResourceType.Human, new Resource(ResourceType.Human, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Wood, new Resource(ResourceType.Wood, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Building, new Resource(ResourceType.Building, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Fish, new Resource(ResourceType.Fish, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Meat, new Resource(ResourceType.Meat, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Mushrooms, new Resource(ResourceType.Mushrooms, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.Stone, new Resource(ResourceType.Stone, earnCallBack, spendCallBack));
        _resources.Add(ResourceType.SpellCard, new Resource(ResourceType.SpellCard, earnCallBack, spendCallBack));
    }

    public void EarnResource(ResourceType resourceType, int value, bool needCallBack = true)
    {
        if (_resources.TryGetValue(resourceType, out var resource))
        {
            resource.EarnResource(value, needCallBack);
        }
    }

    public void SpendResource(ResourceType resourceType, int value)
    {
        if (_resources.TryGetValue(resourceType, out var resource))
        {
            resource.SpendResource(value);
        }
    }

    public Resource GetResourceByType(ResourceType resourceType)
    {
        return _resources[resourceType];
    }
}
