using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArtefactsManager
{
    private List<Artefact> _artefacts;
    private List<Ingredient> _ingredients;

    private IngredientsScriptableObject _availableIngredients;

    public ArtefactsManager(ArtefactsScriptableObject artefactsScriptableObject, IngredientsScriptableObject ingredientsScriptableObject)
    {
        _artefacts = new List<Artefact>();
        _ingredients = new List<Ingredient>();

        _availableIngredients = ingredientsScriptableObject;

    }

    public Ingredient CreateRandomIngredient()
    {
        var item = _availableIngredients.Ingredients[Random.Range(0, _availableIngredients.Ingredients.Count)];
        return new Ingredient(item.Name, item.Description, item.Icon);
    }
}
