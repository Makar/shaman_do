using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class CataclysmManager
{
    public Action<float, float> OnUpdateProgress;

    private Queue<Cataclysm> _availableCataclysms;
    private List<Cataclysm> _activeCataclysms;
    private GameManager _gameManager;

    public CataclysmManager(GameManager gameManager)
    {
        _gameManager = gameManager;
        _availableCataclysms = new Queue<Cataclysm>();
        _activeCataclysms = new List<Cataclysm>();

        var resiurces = new Dictionary<ResourceType, int>();

        resiurces.Add(ResourceType.Human, 15);
        resiurces.Add(ResourceType.Meat, 20);
        resiurces.Add(ResourceType.Mushrooms, 20);
        resiurces.Add(ResourceType.Stone, 20);
        resiurces.Add(ResourceType.Wood, 20);
        resiurces.Add(ResourceType.Fish, 20);

        _availableCataclysms.Enqueue(new Cataclysm(300f, 20, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(25, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(300f, 25, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(35, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(200f, 35, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(45, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(200f, 45, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(55, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(100f, 55, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(65, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(100f, 65, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(75, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(100f, 75, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(85, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(100f, 85, new Dictionary<ResourceType, int>(resiurces)));
        AddResources(95, resiurces);
        _availableCataclysms.Enqueue(new Cataclysm(100f, 100, new Dictionary<ResourceType, int>(resiurces)));

        _activeCataclysms.Add(_availableCataclysms.Dequeue());
    }

    private void AddResources(int value, Dictionary<ResourceType, int> resiurces)
    {
        resiurces[ResourceType.Human] = value;
        resiurces[ResourceType.Mushrooms] = value;
        resiurces[ResourceType.Meat] = value;
        resiurces[ResourceType.Stone] = value;
        resiurces[ResourceType.Wood] = value;
        resiurces[ResourceType.Fish] = value;
    }

    public void Update(float deltaTime)
    {
        var cataclysms = new List<Cataclysm>(_activeCataclysms);

        foreach (var cataclysm in cataclysms)
        {
            cataclysm.Update(deltaTime);

            OnUpdateProgress.Invoke(cataclysm.GetProgress(), cataclysm.GetFreezingTimer());

            if (cataclysm.IsReady())
            {
                ApplyCataclysm(cataclysm);
                _activeCataclysms.Remove(cataclysm);
                if (_availableCataclysms.Count != 0)
                {
                    _activeCataclysms.Add(_availableCataclysms.Dequeue());
                }
            }
        }
    }

    public void AddFreezingTime(float time)
    {
        foreach (var cataclysm in _activeCataclysms)
        {
            cataclysm.AddFreezingTime(time);
        }
    }

    private void ApplyCataclysm(Cataclysm cataclysm)
    {
        _gameManager.DestroyBuildings(cataclysm.GetBuildingsCount());
        _gameManager.SpedResources(cataclysm.GetResources());
    }
}
