using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCell
{
    public int Index { get; private set; }

    private Building _building;

    public MapCell(int index)
    {
        Index = index;
    }

    public bool IsBusy => _building != null;

    public void RemoveBuilding()
    {
        _building = null;
    }

    public void SetBuilding(Building building)
    {
        _building = building;
    }

    public Building GetBuilding()
    {
        return _building;
    }
}
