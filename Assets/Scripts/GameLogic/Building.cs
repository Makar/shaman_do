using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class Building
{
    public enum BuildingType
    {
        Hut,
        Bonfire,
        Crap
    }

    public bool IsFull => _buildingType == BuildingType.Bonfire || _buildingType == BuildingType.Crap || _humans.Count >= _maxHumans;

    private BuildingType _buildingType;
    private Resource _resource;
    private List<Human> _humans;
    private int _resourcePerHuman = 1;
    private float _baseDelay = 2.0f;
    private float _bonusDelay = 0.0f;
    private float _timer;
    private int _maxHumans = 4;

    public Building(BuildingType buildingType, Resource resource)
    {
        _buildingType = buildingType;
        _humans = new List<Human>();
        _resource = resource;
        _baseDelay = GameSettings.GetInstance().GetDelayByType(_resource.ResourceType);

        float hcount = Random.Range(0f, 3f);
        if (hcount > 0) {
            for (int i = 1; i < hcount; i++) {
                AddHuman(new Human());
            } 
        }
    }

    public void Update(float deltaTime)
    {
        _timer += deltaTime;

        var totalDelay = _bonusDelay > 0f ? Mathf.Clamp(_baseDelay * _bonusDelay, 0, 100) : _baseDelay;

        if (_timer >= totalDelay)
        {
            if (_buildingType == BuildingType.Hut)
            {
                var multiplyer = GameSettings.GetInstance().GetMultiplyerByType(_resource.ResourceType);
                _resource.EarnResource(Mathf.Max(1, Mathf.RoundToInt(multiplyer * _resourcePerHuman)) * _humans.Count);
            }
            else if (_buildingType == BuildingType.Bonfire)
            {
                _resource.EarnResource(1);
            }
            _timer -= totalDelay;
        }
    }

    public string GetInfo()
    {
        var multiplyer = GameSettings.GetInstance().GetMultiplyerByType(_resource.ResourceType);
        var description = "Gather resources\n";
        var delay = _bonusDelay > 0f ? Mathf.Clamp(_baseDelay * _bonusDelay, 0, 100) : _baseDelay;
        var stats = $"Delay {delay} \nResources = {_resourcePerHuman} * {multiplyer} * {_humans.Count} + = {Mathf.Max(1, Mathf.RoundToInt(multiplyer * _resourcePerHuman)) * _humans.Count} " +
            $"\n(per person) * k * residens";
        

        return description + stats;
    }

    public void ApplySpell(Spell spell)
    {
        _resourcePerHuman += spell.CountBonus * spell.CountMultiplyerBonus;
        _bonusDelay += spell.DelayBonus;

        if (spell.Resource != null)
        {
            ChangeResource(spell.Resource);
        }
    }

    public void ChangeResource(Resource resource)
    {
        _resource = resource;
        _baseDelay = GameSettings.GetInstance().GetDelayByType(_resource.ResourceType);
    }

    public ResourceType GetResourceType()
    {
        return _resource.ResourceType;
    }

    public void AddHuman(Human human)
    {
        _humans.Add(human);
    }

    public void RemoveHuman()
    {
        if (_humans != null && _humans.Count > 0)
        {
            _humans.RemoveAt(0);
        }
    }

    public void RemoveAll()
    {
        _humans.Clear();
    }

    public int GetHumansCount()
    {
        return _humans.Count;
    }

    public int GetFreeSpaces()
    {
        return _maxHumans - _humans.Count;
    }
}
