using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD50.Resources
{
    public enum ResourceType
    {
        Undefined,
        SpellCard,
        Wood,
        Fish,
        Meat,
        Human,
        Building,
        Mushrooms,
        Stone,
    }
}