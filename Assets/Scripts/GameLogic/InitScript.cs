using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitScript : MonoBehaviour
{
    [SerializeField] private PointListController _pointListController;
    [SerializeField] private MapView _mapView;
    [SerializeField] private ResourcesView _resourcesView;
    [SerializeField] private BuildingInfoView _buildingInfoView;
    [SerializeField] private CardsView _cardsView;
    [SerializeField] private CataclysmView _cataclysmView;
    [SerializeField] private TestMover _mover;
    [SerializeField] private BotsManager _botsManager;
    [SerializeField] private ResultsView _resultsView;
    [SerializeField] private GameObject _cataclysmAnimation;
    [SerializeField] private SpellCardsScriptableObject _spellsScriptableObject;
    [SerializeField] private IngredientsScriptableObject _ingredientsScriptableObject;
    [SerializeField] private ArtefactsScriptableObject _artefactsScriptableObject;

    private GameManager _gameManager;
    private GameViewManager _gameViewManager;

    public void Start()
    {
        _gameManager = new GameManager(_pointListController, _spellsScriptableObject, _artefactsScriptableObject, _ingredientsScriptableObject);
        _gameViewManager = new GameViewManager(_mapView, _resourcesView, _buildingInfoView, _gameManager.GetMap());

        _gameManager.OnBuildingCreated += _gameViewManager.OnBuildingCreated;
        _gameManager.OnBuildingUpdated += _gameViewManager.OnBuildingUpdated;
        _gameManager.OnResourceUpdated += _gameViewManager.OnResourceUpdated;
        _gameManager.OnResourceUpdated += _cardsView.UpdateAvailableResources;
        _gameManager.OnBuildingRemoved += _gameViewManager.OnBuildingDestroyed;
        _gameManager.OnCardCreated += _cardsView.OnCardCreated;
        _gameManager.GetCataclysmManager().OnUpdateProgress += _cataclysmView.UpdateProgress;
        _gameManager.OnSpellCast += _mapView.OnSpellCast;

        _gameManager.OnHumanCreated += _botsManager.AddBot;
        _gameManager.OnHumanRemoved += _botsManager.RemoveBot;

        _gameManager.OnShowResults += _resultsView.Show;

        _gameManager.StartCataclysm += StartCataclysm;

        _cardsView.OnCreateSpell += _gameManager.CreateSpell;
        _cardsView.OnSpellCreated += _mover.PrepareSpell;
        _mover.OnSpellActivated += _gameManager.CastSpell;


        _gameManager.Init();
    }

    public void Update()
    {
        _gameManager.Tick();
    }

    public void StartCataclysm()
    {
        StartCoroutine(PlayerCataclysm());
    }

    private IEnumerator PlayerCataclysm()
    {
        _cataclysmAnimation.SetActive(true);

        yield return new WaitForSeconds(3f);

        _cataclysmAnimation.SetActive(false);

    }
}
