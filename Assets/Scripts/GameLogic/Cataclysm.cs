using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;

public class Cataclysm
{
    private Dictionary<ResourceType, int> _resources;
    private int _buildingsCount;
    private float _delay;
    private float _timer;
    private float _freezingTimer;

    private bool _isReady;

    public Cataclysm(float delay, int buildingsCount, Dictionary<ResourceType, int> resources)
    {
        _delay = delay;
        _buildingsCount = buildingsCount;
        _resources = resources;
    }

    public Dictionary<ResourceType, int> GetResources()
    {
        return _resources;
    }

    public void Update(float deltaTime)
    {
        if (_freezingTimer > 0)
        {
            _freezingTimer -= deltaTime;
            deltaTime *= 0.5f;
        }

        _timer += deltaTime;

        if (_timer >= _delay)
        {
            _isReady = true;
        }
    }

    public void AddFreezingTime(float value)
    {
        _freezingTimer += value;
    }

    public float GetFreezingTimer()
    {
        return _freezingTimer;
    }

    public int GetBuildingsCount()
    {
        return _buildingsCount;
    }

    public bool IsReady()
    {
        return _isReady;
    }

    public float GetProgress()
    {
        return _timer / _delay;
    }

}
