using LD50.Resources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellsManager
{
    private static int CardId;

    private List<SpellCard> _spellCards;
    private List<SpellCard> _availableCards;

    private ResourceManager _resourceManager;
    private CataclysmManager _cataclysmManager;

    private Queue<int> _predefinedCards;

    private const int LOW = 5;
    private const int MID = 5;
    private const int ALOT = 5;

    public SpellsManager(ResourceManager resourceManager, CataclysmManager cataclysmManager, SpellCardsScriptableObject spellsScriptableObject)
    {
        _spellCards = new List<SpellCard>();
        _availableCards = new List<SpellCard>();
        _predefinedCards = new Queue<int>();
        _resourceManager = resourceManager;
        _cataclysmManager = cataclysmManager;


        foreach (var spellData in spellsScriptableObject.Spells)
        {
            spellData.Init();
            SpellCard spellCard = new SpellCard(CardId++, spellData.Info, spellData.CataclysmTime, 
                                                spellData.CountMultiplyerBonus, spellData.CountBonus, 
                                                spellData.DelayBonus, spellData.Weight, 
                                                spellData.ResourceType, spellData.RequiredResources);
            _availableCards.Add(spellCard);
        }

        ////Resources
        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, ALOT);

        //    SpellCard spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Building, requiredResources);
        //    spellCard.Info = "< ARCHITECT >\\n \\n Transform residents to builders";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, LOW);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Fish, requiredResources);
        //    spellCard.Info = "< MARINARA >\\n \\nTransform residents to fishermen";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, ALOT);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Human, requiredResources);
        //    spellCard.Info = "< INSEMINATOR >\\n \\nTransform residents to lovers";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);
        //    _predefinedCards.Enqueue(_availableCards.Count - 1);
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, LOW);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Meat, requiredResources);
        //    spellCard.Info = "< BURGERQUIN >\\n \\nTransform residents to hunters";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, LOW);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Wood, requiredResources);
        //    spellCard.Info = "< DRUID >\\n \\nTransform residents to lumbermen";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Meat, LOW);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, -0.0f, ResourceType.Stone, requiredResources);
        //    spellCard.Info = "< EXCAVATOR >\\n \\nTransform residents to miners";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        ////buffs
        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Wood, LOW);            

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 2, -0.0f, ResourceType.Undefined, requiredResources);            
        //    spellCard.Info = "< INSPIRE >\\n \\n upgrade geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            

        //    spellCard = new SpellCard(CardId++, 0f, 1, 2, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< INSPIRE >\\n \\n upgrade geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 1, 2, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< INSPIRE >\\n \\n upgrade geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 1, 2, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< INSPIRE >\\n \\n upgrade geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);
        //    _predefinedCards.Enqueue(_availableCards.Count - 1);
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Wood, (int)(LOW*3*1.5f));

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 5, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< UPGRADE >\\n \\n geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 1, 5, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< UPGRADE >\\n \\n geathering count per person";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}
        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Wood, 10);

        //    var spellCard = new SpellCard(CardId++, 0f, 1, 0, 0.8f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< RUN >\\n \\n speed up gathering";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 1, 0, 0.8f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< RUN >\\n \\n speed up gathering";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 1, 0, 0.8f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< RUN >\\n \\n speed up gathering";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);
        //}


        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Mushrooms, LOW);

        //    var spellCard = new SpellCard(CardId++, 0f, 2, 0, 0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< SMOKE >\\n \\n multiply card effect x2";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");            
        //    _availableCards.Add(spellCard);

        //    spellCard = new SpellCard(CardId++, 0f, 2, 0, 0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< SMOKE >\\n \\n multiply card effect x2";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);

        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Mushrooms, ALOT);

        //    var spellCard = new SpellCard(CardId++, 10f, 2, 0, -1.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< SMOKE >\\n \\n multiply card effect x5";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);
        //}



        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Stone, 50);

        //    var spellCard = new SpellCard(CardId++, 30f, 1, 0, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< DELAY >\\n \\n stop IT a bit!!!";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}

        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Fish, 20);
        //    var spellCard = new SpellCard(CardId++, 30f, 1, 0, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< DELAY >\\n \\n stop IT a bit!!!";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);
        //}
        //{
        //    var requiredResources = new Dictionary<ResourceType, int>();
        //    requiredResources.Add(ResourceType.Stone, 100);

        //    var spellCard = new SpellCard(CardId++, 120f, 1, 0, -0.0f, ResourceType.Undefined, requiredResources);
        //    spellCard.Info = "< DELAY >\\n \\n stop IT a lot!!!";
        //    spellCard.Info = spellCard.Info.Replace("\\n", "\n");
        //    _availableCards.Add(spellCard);            
        //}


    }

    public void CastSpell(List<MapCell> cells, Spell spell)
    {
        foreach(var cell in cells)
        {
             cell.GetBuilding()?.ApplySpell(spell);
        }

        _cataclysmManager.AddFreezingTime(spell.CataclysmTime);
    }

    public Spell CreateSpell(List<int> ids)
    {
        var spell = new Spell(_resourceManager);
        var cards = new List<SpellCard>(_spellCards);

        foreach (var id in ids)
        {
            foreach (var card in cards)
            {
                if (card.Id == id)
                {
                    spell.ApplySpellCard(card);
                    _spellCards.Remove(card);
                    break;
                }
            }
        }

        return spell;
    }

    public SpellCard AddRandomCard()
    {
        int sumOfWeight = 0;

        foreach(var spellCard in _availableCards)
        {
            sumOfWeight += spellCard.Weight;
        }

        int randomWeight = Random.Range(0, sumOfWeight);

        foreach (var spellCard in _availableCards)
        {
            if (randomWeight < spellCard.Weight)
            {
                _spellCards.Add(spellCard);
                return spellCard;
            }
            else
            {
                randomWeight -= spellCard.Weight;
            }
        }

        return null;
    }

    public void AddCard(SpellCard spellCard)
    {
        _spellCards.Add(spellCard);
    }

    public void RemoveCard(SpellCard spellCard)
    {
        _spellCards.Remove(spellCard);
    }
}
