using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LD50.Resources;
using System.Linq;

public class Map
{
    private int _size;
    private List<MapCell> _cells;

    public Map(int size)
    {
        _size = size;
        _cells = new List<MapCell>();

        for(int i = 0; i < _size; i++)
        {
            _cells.Add(new MapCell(i));
        }
    }

    public int GetBuildingsFreeSpaces()
    {
        return _cells.Sum(x => x.IsBusy ? x.GetBuilding().GetFreeSpaces() : 0);
    }

    public List<MapCell> GetCells()
    {
        return _cells;
    }

    public void CreateBuilding(int index, Building.BuildingType buildingType, Resource resource)
    {
        _cells[index].SetBuilding(new Building(buildingType, resource));
    }

    public void DestroyBuilding(int index)
    {
        _cells[index].RemoveBuilding();
    }

    public void MoveBuilding(int from, int to)
    {
        var building = _cells[from].GetBuilding();
        _cells[from].RemoveBuilding();
        _cells[to].SetBuilding(building);
    }

    public MapCell GetCell(int index)
    {
        return _cells[index];
    }
}
