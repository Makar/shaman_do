using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpellCardsScriptableObject")]
public class SpellCardsScriptableObject : ScriptableObject
{
    public List<SpellCardScriptableObject> Spells;
}
