using LD50.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpellCardScriptableObject")]
public class SpellCardScriptableObject : ScriptableObject
{
    public string Info;
    public float CataclysmTime;
    public int CountMultiplyerBonus;
    public int CountBonus;
    public float DelayBonus;
    public int Weight;
    public ResourceType ResourceType;
    public List<SpellCardRequiredResource> Resources = new List<SpellCardRequiredResource>();
    public Dictionary<ResourceType, int> RequiredResources = new Dictionary<ResourceType, int>();

    public void Init()
    {
        foreach(var resource in Resources)
        {
            if (RequiredResources.TryGetValue(resource.ResourceType, out var value))
            {
                RequiredResources[resource.ResourceType] = value + resource.Count;
            }
            else
            {
                RequiredResources.Add(resource.ResourceType, resource.Count);
            }
        }
    }
}

[Serializable]
public class SpellCardRequiredResource
{
    public int Count;
    public ResourceType ResourceType;
}
