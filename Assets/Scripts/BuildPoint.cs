using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildPoint : MonoBehaviour
{
    // Start is called before the first frame update
    void Start(){
        
    }

    // Update is called once per frame
    void OnDrawGizmosSelected(){
        var currentPos = transform.position;
        transform.position = new Vector3(Mathf.Round(currentPos.x / 2f) *2f,
                                     Mathf.Round(currentPos.y / 2f) *2f,
                                     Mathf.Round(currentPos.z /2f) * 2f
                                     );
    }
    
}
