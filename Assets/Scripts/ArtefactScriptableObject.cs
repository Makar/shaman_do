using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ArtefactScriptableObject")]

public class ArtefactScriptableObject : ScriptableObject
{
    public string Name;
    public string Description;
    public Sprite Icon;

    public List<Ingredient.IngredientType> Ingredients;
}
