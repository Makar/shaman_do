using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/IngredientsScriptableObject")]

public class IngredientsScriptableObject : ScriptableObject
{
    public List<IngredientScriptableObject> Ingredients;
}
