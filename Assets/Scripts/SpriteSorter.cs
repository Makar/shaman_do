using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSorter : MonoBehaviour
{
    private Renderer spr;
    // Start is called before the first frame update
    void Start() {
        spr = GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update() {
        spr.sortingOrder = -(int)transform.position.y * 10;
    }

    private void OnDrawGizmosSelected() {
        if (spr == null) {
            spr = GetComponent<Renderer>();
        }
        spr.sortingOrder = -(int)(transform.position.y * 10);
    }

}
