using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointListController : MonoBehaviour
{
    public BuildPoint[] buildPoints;
    // Start is called before the first frame update

    public bool clearList = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void OnDrawGizmosSelected() {
        if (clearList == true) {
            clearList = false;            
            buildPoints = 
                GetComponentsInChildren<BuildPoint>();
        }
    }
}
