using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/IngredientScriptableObject")]

public class IngredientScriptableObject : ScriptableObject
{
    public string Name;
    public string Description;
    public Sprite Icon;
    public Ingredient.IngredientType IngredientType;
}
