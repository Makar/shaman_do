using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardBuffItem : MonoBehaviour
{
    [SerializeField] private Text _label;

    public void UpdateInfo(string info)
    {
        _label.text = info;
    }
}
