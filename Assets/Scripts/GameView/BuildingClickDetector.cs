using System;
using UnityEngine;

public class BuildingClickDetector : MonoBehaviour
{
    public Action OnMouseDownCallback;
    public Action OnMouseUpCallback;

    void OnMouseDown()
    {
        OnMouseDownCallback?.Invoke();
    }

    void OnMouseUp()
    {
        OnMouseUpCallback?.Invoke();
    }
}
