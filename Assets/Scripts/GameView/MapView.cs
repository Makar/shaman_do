using LD50.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapView : MonoBehaviour
{
    public Action<int> ShowBuildingInfo;
    public Action HideBuildingInfo;

    [SerializeField] private PointListController _pointListController;
    [SerializeField] private BuildingView _hutPrefab;
    [SerializeField] private BuildingView _bunfirePrefab;


    public void CreateHut(int cellIndex, int humansCount, ResourceType resourceType)
    {
        var building = Instantiate(_hutPrefab, _pointListController.buildPoints[cellIndex].transform);
        building.Init(cellIndex);
        building.UpdateBuilding(humansCount, resourceType);

        building.ShowInfo += ShowBuildingInfo;
        building.HideInfo += HideBuildingInfo;
    }

    public void UpdateHut(int cellIndex, int humansCount, ResourceType resourceType)
    {
        var building = _pointListController.buildPoints[cellIndex].GetComponentInChildren<BuildingView>();
        if (building != null)
        {
            building.UpdateBuilding(humansCount, resourceType);
        }
    }

    public void CreateBonfire(int cellIndex)
    {
        var building = Instantiate(_bunfirePrefab, _pointListController.buildPoints[cellIndex].transform);
        building.Init(cellIndex);

        building.ShowInfo += ShowBuildingInfo;
        building.HideInfo += HideBuildingInfo;
    }

    public void RemoveBuilding(int index)
    {
        var building = _pointListController.buildPoints[index].GetComponentInChildren<BuildingView>();
        if (building != null)
        {
            Destroy(building.gameObject);
        }
    }

    public void OnSpellCast(List<int> cells)
    {
        foreach(var cellIndex in cells)
        {
            var building = _pointListController.buildPoints[cellIndex].GetComponentInChildren<BuildingView>();
            if (building != null)
            {
                building.PlaySpellEffect();
            }
        }
    }
}
