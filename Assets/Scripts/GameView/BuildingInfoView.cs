using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingInfoView : MonoBehaviour
{
    [SerializeField] private Text _infoLabel;

    public void UpdateInfo(string info)
    {
        _infoLabel.text = info;
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
