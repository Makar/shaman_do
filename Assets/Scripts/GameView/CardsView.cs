using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LD50.Resources;

public class CardsView : MonoBehaviour
{
    public Action<List<int>> OnCreateSpell;
    public Action OnSpellCreated;

    [SerializeField] private Button _openButton;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _createButton;
    [SerializeField] private CardView _cardPrefab;
    [SerializeField] private GameObject _container;
    [SerializeField] private GameObject _spellContainer;
    [SerializeField] private GameObject _window;

    [SerializeField] private SpellResourceIcon _humanResource;
    [SerializeField] private SpellResourceIcon _meatResource;
    [SerializeField] private SpellResourceIcon _fishResource;
    [SerializeField] private SpellResourceIcon _mushroomsResource;
    [SerializeField] private SpellResourceIcon _woodResource;
    [SerializeField] private SpellResourceIcon _stoneResource;

    private List<CardView> _cards;
    private List<CardView> _spellCards;

    private Dictionary<ResourceType, int> _availableResources;

    private Dictionary<ResourceType, SpellResourceIcon> _iconsByType;

    public void Start()
    {
        if (_cards == null)
        {
            _cards = new List<CardView>();
        }

        _spellCards = new List<CardView>();
        _iconsByType = new Dictionary<ResourceType, SpellResourceIcon>();

        _openButton.onClick.AddListener(Open);
        _closeButton.onClick.AddListener(Close);
        _createButton.onClick.AddListener(CreateSpell);

        _iconsByType.Add(ResourceType.Human, _humanResource);
        _iconsByType.Add(ResourceType.Meat, _meatResource);
        _iconsByType.Add(ResourceType.Fish, _fishResource);
        _iconsByType.Add(ResourceType.Mushrooms, _mushroomsResource);
        _iconsByType.Add(ResourceType.Wood, _woodResource);
        _iconsByType.Add(ResourceType.Stone, _stoneResource);

        _createButton.gameObject.SetActive(false);
    }

    public void OnCardCreated(int id, string name, float delay, int resourcesCount, int resourcesMultiplyer, float speed, ResourceType resourceType, Dictionary<ResourceType, int> resources)
    {
        var cardView = Instantiate(_cardPrefab, _container.transform);
        cardView.Init(id, name, delay, resourcesCount, resourcesMultiplyer, speed, resourceType, resources);
        cardView.OnCardClick += OnCardClick;

        if (_cards == null)
        {
            _cards = new List<CardView>();
        }

        _cards.Add(cardView);
    }

    public void UpdateAvailableResources(ResourceType resourceType, int count)
    {
        if (_availableResources == null)
        {
            _availableResources = new Dictionary<ResourceType, int>();
        }

        if (_availableResources.ContainsKey(resourceType))
        {
            _availableResources[resourceType] = count;
        }
        else
        {
            _availableResources.Add(resourceType, count);
        }
    }

    public void OnCardRemoved(int id)
    {
        foreach(var card in _cards)
        {
            if (card.GetId() == id)
            {
                _cards.Remove(card);
                Destroy(card.gameObject);
                return;
            }
        }
    }

    private void MoveCardToSpell(CardView cardView)
    {
        if (_spellCards.Count < 3)
        {
            _spellCards.Add(cardView);
            _cards.Remove(cardView);
            cardView.transform.parent = _spellContainer.transform;

            UpdateSpellInfo();
            cardView.ApproveMoving();
        }
    }

    private void MoveCardBack(CardView cardView)
    {
        _spellCards.Remove(cardView);
        _cards.Add(cardView);
        cardView.transform.parent = _container.transform;

        UpdateSpellInfo();
        cardView.ApproveMoving();
    }

    private void OnCardClick(bool inSpell, CardView cardView)
    {
        if (inSpell)
        {
            MoveCardToSpell(cardView);
        }
        else
        {
            MoveCardBack(cardView);
        }
    }

    private void Open()
    {
        _window.SetActive(true);
    }

    private void Close()
    {
        _window.SetActive(false);
    }

    private void CreateSpell()
    {
        var ids = new List<int>();

        foreach(var card in _spellCards)
        {
            ids.Add(card.GetId());
        }

        if (ids.Count == 0)
        {
            return;
        }

        OnCreateSpell(ids);
        OnSpellCreated();

        Close();

        foreach (var card in _spellCards)
        {
            Destroy(card.gameObject);
        }

        _spellCards.Clear();
        HideAllResources();
    }

    private void UpdateSpellInfo()
    {
        HideAllResources();

        var requiredResources = new Dictionary<ResourceType, int>();
        _createButton.gameObject.SetActive(_spellCards.Count > 0);

        foreach (var cardVew in _spellCards)
        {
            foreach(var resource in cardVew.GetResources())
            {
                if (_iconsByType.TryGetValue(resource.Key, out var icon))
                {
                    icon.Show();
                    icon.HideError();
                    icon.AddCount(resource.Value);

                    if (requiredResources.ContainsKey(resource.Key))
                    {
                        requiredResources[resource.Key] += resource.Value;
                    }
                    else
                    {
                        requiredResources.Add(resource.Key, resource.Value);
                    }

                    if (!_availableResources.TryGetValue(resource.Key, out var resourceCount) || resourceCount < requiredResources[resource.Key])
                    {
                        icon.ShowError();
                        _createButton.gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    private void HideAllResources()
    {
        _humanResource.Hide();
        _meatResource.Hide();
        _fishResource.Hide();
        _mushroomsResource.Hide();
        _woodResource.Hide();
        _stoneResource.Hide();
    }
}
