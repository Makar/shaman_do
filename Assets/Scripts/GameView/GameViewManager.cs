using LD50.Resources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameViewManager
{
    private MapView _mapView;
    private ResourcesView _resourcesView;
    private BuildingInfoView _buildingInfoView;
    private Map _map;

    public GameViewManager(MapView mapView, ResourcesView resourcesView, BuildingInfoView buildingInfoView, Map map)
    {
        _mapView = mapView;
        _resourcesView = resourcesView;
        _buildingInfoView = buildingInfoView;
        _map = map;

        _mapView.ShowBuildingInfo += ShowBuildingInfo;
        _mapView.HideBuildingInfo += HideBuildingInfo;
    }

    public void ShowBuildingInfo(int cellIndex)
    {
        var cell = _map.GetCell(cellIndex);
        var building = cell.GetBuilding();

        if (building == null)
        {
            return;
        }

        _buildingInfoView.transform.position = Input.mousePosition;
        _buildingInfoView.UpdateInfo(building.GetInfo());
        _buildingInfoView.Show();
    }

    public void HideBuildingInfo()
    {
        _buildingInfoView.Hide();
    }

    public void OnBuildingCreated(Building.BuildingType buildingType, int cellIndex, int humansCount, ResourceType resourceType)
    {
        if (buildingType == Building.BuildingType.Hut)
        {
            _mapView.CreateHut(cellIndex, humansCount, resourceType);
        }

        if (buildingType == Building.BuildingType.Bonfire)
        {
            _mapView.CreateBonfire(cellIndex);
        }
    }

    public void OnBuildingUpdated(ResourceType resourceType, int cellIndex, int humansCount)
    {
        _mapView.UpdateHut(cellIndex, humansCount, resourceType);
    }

    public void OnBuildingDestroyed(int index)
    {
        _mapView.RemoveBuilding(index);
    }

    public void OnResourceUpdated(ResourceType resourceType, int value)
    {
        _resourcesView.UpdateResource(resourceType, value);
    }

}
