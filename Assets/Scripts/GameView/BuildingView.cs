using LD50.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BuildingView : MonoBehaviour
{
    public Action<int> ShowInfo;
    public Action HideInfo;

    [SerializeField] private GameObject _humanIcon;
    [SerializeField] private GameObject _meatIcon;
    [SerializeField] private GameObject _fishIcon;
    [SerializeField] private GameObject _mushroomsIcon;
    [SerializeField] private GameObject _woodIcon;
    [SerializeField] private GameObject _stoneIcon;
    [SerializeField] private GameObject _buildingsIcon;
    [SerializeField] private TextMeshPro text;
    [SerializeField] private BuildingClickDetector _buildingClickDetector;
    [SerializeField] private Animator _spellEffect;

    private int _cellIndex;

    public void Start()
    {
        if (_buildingClickDetector != null)
        {
            _buildingClickDetector.OnMouseDownCallback += OnMouseDown;
            _buildingClickDetector.OnMouseUpCallback += OnMouseUp;
        }

        _spellEffect.speed = 0f;
        _spellEffect.gameObject.SetActive(false);

    }

    public void Init(int cellIndex)
    {
        _cellIndex = cellIndex;
    }

    public void UpdateBuilding(int humansCount, ResourceType resourceType)
    {
        if (text == null)
        {
            return;
        }

        text.text = humansCount.ToString();

        UpdateIcon(resourceType);
    }

    public void PlaySpellEffect()
    {
        StartCoroutine(PlayingSpellEffect());
    }

    private IEnumerator PlayingSpellEffect()
    {
        yield return new WaitForSeconds(0.3f);

        _spellEffect.speed = 1f;
        _spellEffect.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.6f);

        _spellEffect.speed = 0f;
        _spellEffect.gameObject.SetActive(false);
    }

    void OnMouseDown()
    {
        ShowInfo?.Invoke(_cellIndex);
    }

    void OnMouseUp()
    {
        HideInfo?.Invoke();
    }

    private void UpdateIcon(ResourceType resourceType)
    {
        _humanIcon.SetActive(false);
        _meatIcon.SetActive(false);
        _fishIcon.SetActive(false);
        _mushroomsIcon.SetActive(false);
        _woodIcon.SetActive(false);
        _stoneIcon.SetActive(false);
        _buildingsIcon.SetActive(false);

        if (resourceType == ResourceType.Human)
        {
            _humanIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Meat)
        {
            _meatIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Fish)
        {
            _fishIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Mushrooms)
        {
            _mushroomsIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Wood)
        {
            _woodIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Stone)
        {
            _stoneIcon.SetActive(true);
        }

        if (resourceType == ResourceType.Building)
        {
            _buildingsIcon.SetActive(true);
        }
    }
}
