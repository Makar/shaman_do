using LD50.Resources;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotsManager : MonoBehaviour
{
    [SerializeField] private BotView _botPrefab;
    [SerializeField] private PointListController _pointListController;

    private List<BotView> _bots;

    public void AddBot()
    {
        if (_bots == null)
        {
            _bots = new List<BotView>();
        }

        var position = _pointListController.buildPoints[Random.Range(0, _pointListController.buildPoints.Length)].transform.position;
        var bot = Instantiate(_botPrefab, transform);
        bot.transform.position = position;

        _bots.Add(bot);
    }

    public void RemoveBot()
    {
        if (_bots.Count > 0)
        {
            Destroy(_bots[0].gameObject);
            _bots.RemoveAt(0);
        }
    }
}
