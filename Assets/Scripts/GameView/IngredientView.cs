using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientView : MonoBehaviour
{
    public Action<bool, CardView> OnClick;

    [SerializeField] private Sprite _icon;

    private int _id;

    public void Init(int id, Sprite icon)
    {
        _id = id;
        _icon = icon;
    }
}
