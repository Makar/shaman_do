using LD50.Resources;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour
{
    public Action<bool, CardView> OnCardClick;

    [SerializeField] private Text _nameLabel;
    [SerializeField] private Button _moveButton;

    [SerializeField] private CardBuffItem _delayCataclysmItem;
    [SerializeField] private CardBuffItem _resourceItem;
    [SerializeField] private CardBuffItem _cardResourceItem;
    [SerializeField] private CardBuffItem _speedResourceItem;
    [SerializeField] private CardBuffItem _resourceHuman;
    [SerializeField] private CardBuffItem _resourceMeat;
    [SerializeField] private CardBuffItem _resourceFish;
    [SerializeField] private CardBuffItem _resourceMushroom;
    [SerializeField] private CardBuffItem _resourceWood;
    [SerializeField] private CardBuffItem _resourceStone;
    [SerializeField] private CardBuffItem _resourceBuildings;


    private Dictionary<ResourceType, int> _resources;
    private Dictionary<ResourceType, CardBuffItem> _iconsByType;

    private int _id;
    private bool _inSpell;

    public void Init(int id, string name, float delay, int resourcesCount, int resourcesMultiplyer, float speed, ResourceType resourceType,  Dictionary<ResourceType, int> resources)
    {
        _id = id;
        _nameLabel.text = name;
        _resources = resources;

        _iconsByType = new Dictionary<ResourceType, CardBuffItem>();

        _iconsByType.Add(ResourceType.Human, _resourceHuman);
        _iconsByType.Add(ResourceType.Meat, _resourceMeat);
        _iconsByType.Add(ResourceType.Fish, _resourceFish);
        _iconsByType.Add(ResourceType.Mushrooms, _resourceMushroom);
        _iconsByType.Add(ResourceType.Wood, _resourceWood);
        _iconsByType.Add(ResourceType.Stone, _resourceStone);
        _iconsByType.Add(ResourceType.Building, _resourceBuildings);

        if (delay > 0)
        {
            _delayCataclysmItem.UpdateInfo("Delay cataclysm " + delay);
            _delayCataclysmItem.gameObject.SetActive(true); ;
        }

        if (resourcesCount > 0)
        {
            _resourceItem.UpdateInfo("All resources +" + resourcesCount);
            _resourceItem.gameObject.SetActive(true); ;
        }

        if (resourcesMultiplyer > 1)
        {
            _cardResourceItem.UpdateInfo("Cards resources x" + resourcesMultiplyer);
            _cardResourceItem.gameObject.SetActive(true); ;
        }

        if (speed != 0)
        {
            _speedResourceItem.UpdateInfo("Delay resources " + speed);
            _speedResourceItem.gameObject.SetActive(true); ;
        }

        if (_iconsByType.TryGetValue(resourceType, out var item))
        {
            item.gameObject.SetActive(true);
        }

        _moveButton.onClick.AddListener(OnClick);
    }

    public void OnClick()
    {
        OnCardClick?.Invoke(!_inSpell, this);
    }

    public void ApproveMoving()
    {
        _inSpell = !_inSpell;
    }

    public int GetId()
    {
        return _id;
    }

    public Dictionary<ResourceType, int> GetResources()
    {
        return _resources;
    }
}
