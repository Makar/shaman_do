using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CataclysmView : MonoBehaviour
{
    [SerializeField] private Image _progressImage;
    [SerializeField] private Text _frizingTimer;
    [SerializeField] private Color _color;
    [SerializeField] private Color _freezingColor;

    public void UpdateProgress(float progress, float frizingTimer)
    {
        _progressImage.fillAmount = progress;

        if (frizingTimer > 0)
        {
            _frizingTimer.gameObject.SetActive(true);
            _frizingTimer.text = ((int)frizingTimer).ToString();
            _progressImage.color = _freezingColor;
        }
        else
        {
            _frizingTimer.gameObject.SetActive(false);
            _progressImage.color = _color;
        }
    }
}
