using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotView : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    private Vector2 _movingDirection;
    private Vector2 startPos;
    private float teleportCd = 10;
    private float time = 0;
    public void Start()
    {
        _animator.speed = 1f;
        startPos = transform.position;
        _movingDirection = new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f));
    }

    public void Update()
    {
        time = Time.time;
        if (Time.time>teleportCd) {
            transform.position = startPos;
            teleportCd = Time.time+ Random.Range(-10f, 15f);
        }
        transform.Translate(_movingDirection * 0.005f);
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        _movingDirection = new Vector2(Random.Range(-2f, 2f), Random.Range(-2f, 2f));
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        _movingDirection = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));

    }
}
