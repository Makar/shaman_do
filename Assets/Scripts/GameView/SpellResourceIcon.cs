using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellResourceIcon : MonoBehaviour
{
    [SerializeField] private Text _lavel;
    [SerializeField] private Image _background;
    [SerializeField] private Color _errorColor;

    private int _currentValue;

    public void AddCount(int value)
    {
        _currentValue += value;
        _lavel.text = _currentValue.ToString();
    }

    public void ShowError()
    {
        _background.color = _errorColor;
    }
    public void HideError()
    {
        _background.color = Color.white;

    }

    public void Hide()
    {
        gameObject.SetActive(false);
        _currentValue = 0;
        _lavel.text = _currentValue.ToString();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
