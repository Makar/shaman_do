using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LD50.Resources;

public class ResourcesView : MonoBehaviour
{
    [SerializeField] private Text _humansLabel;
    [SerializeField] private Text _meatLabel;
    [SerializeField] private Text _fishLabel;
    [SerializeField] private Text _mushroomsLabel;
    [SerializeField] private Text _woodLabel;
    [SerializeField] private Text _stoneLabel;

    public void UpdateResource(ResourceType resourceType, int value)
    {
        if (resourceType == ResourceType.Human)
        {
            _humansLabel.text = value.ToString();
        }

        if (resourceType == ResourceType.Meat)
        {
            _meatLabel.text = value.ToString();
        }

        if (resourceType == ResourceType.Fish)
        {
            _fishLabel.text = value.ToString();
        }

        if (resourceType == ResourceType.Wood)
        {
            _woodLabel.text = value.ToString();
        }

        if (resourceType == ResourceType.Mushrooms)
        {
            _mushroomsLabel.text = value.ToString();
        }

        if (resourceType == ResourceType.Stone)
        {
            _stoneLabel.text = value.ToString();
        }
    }
}
