using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultsView : MonoBehaviour
{
    [SerializeField] private Text _resulText;
    [SerializeField] private Button _restartButton;

    public void Start()
    {
        _restartButton.onClick.AddListener(OnRestartClick);
    }

    public void Show(float result)
    {
        float bestTime = PlayerPrefs.GetFloat("BestTime", 0f);
        if (result > bestTime)
        {
            bestTime = result;
            PlayerPrefs.SetFloat("BestTime", bestTime);
        }

        gameObject.SetActive(true);

        TimeSpan bestTimeStamp = TimeSpan.FromSeconds(bestTime);
        string bestText = string.Format("{0:D2}h:{1:D2}m:{2:D2}s", bestTimeStamp.Hours, bestTimeStamp.Minutes, bestTimeStamp.Seconds);

        TimeSpan resultTimeStamp = TimeSpan.FromSeconds(result);
        string resultText = string.Format("{0:D2}h:{1:D2}m:{2:D2}s", resultTimeStamp.Hours, resultTimeStamp.Minutes, resultTimeStamp.Seconds);

        _resulText.text = $"Survaving time {resultText} \nBest time {bestText}";
    }

    private void OnRestartClick()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
}
